# Instructions for various things

Cluster init: `kind create cluster --config=kind.yaml`
Cluster destroy: `kind delete cluster -n rialtic`

Build and deploy stack: `skaffold dev`
- Note: it may take a while for linkerd to come fully online. Watch the output of `linkerd check`. 
- Note: you may need to delete the application and nginx controller pods to get them to actually join the mesh. I suspect the initial ones are created too quickly, before linkerd is ready to inject things. Likely area for improvement later. There's maybe a flag in the helm chart for this?

Requests:
- Send a person: `echo -n '{"FirstName":"John","LastName":"Bologna","BirthDate":"1992-07-02T00:00:00Z"}' | http PUT http://localhost:80/hash`
- Get a person: `http http://localhost:80/hash/5d8542f41a8a9d7a6ed6894c4effcea02af0d49a7ef095ceb781d7bffeaa1ebf`


