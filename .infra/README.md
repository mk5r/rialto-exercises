In an effort to turn this around faster, I am skipping the terraform deployment component, as I'm using skaffold to deploy into the local cluster.

However, for a demonstration of my terraform capabilities, including deployment of IRSA, helm charts, and raw manifests into a kubernetes cluster, please view:
- https://github.com/mkantzer/k8splayground-core-infra
- https://github.com/mkantzer/k8splayground-cluster-state

Note that my kubernetes playground is still undergoing active development. However, it is already deploying a fully functional EKS cluster, complete with ArgoCD, ALB ingress controllers, IRSA, and the like. 