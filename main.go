package main

import (
	"net/http"
	"os"
	"runtime/debug"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/rs/zerolog"
	"gitlab.com/rialtic-external/exercises/v2/handlers"
	"gitlab.com/rialtic-external/exercises/v2/storage"
)

func main() {
	log := zerolog.New(os.Stdout).With().
		Timestamp().
		Logger()

	log.Info().Msg("Initializing filestore...")

	ds, err := storage.New("datastore")
	if err != nil {
		log.Fatal().Err(err).Msg("Unable to initialize filestore")
	}

	router := Routes(log, ds)

	log.Info().Msg("Starting server...")
	if err := http.ListenAndServe(":8080", router); err != nil {
		log.Fatal().Err(err).Msg("Startup failed")
	}

}

func Routes(logger zerolog.Logger, ds storage.DataStore) *chi.Mux {
	router := chi.NewRouter()
	router.Use(middleware.Recoverer)
	router.Use(LoggerMiddleware(&logger))
	router.Use(middleware.Timeout(5 * time.Second))

	router.Put("/hash", handlers.GetPutPersonHandler(ds))
	router.Get("/hash/{personHash}", handlers.GetGetPersonHandler(ds))

	return router
}

// stollen fully from https://github.com/ironstar-io/chizerolog/blob/master/main.go
func LoggerMiddleware(logger *zerolog.Logger) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			log := logger.With().Logger()

			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

			t1 := time.Now()
			defer func() {
				t2 := time.Now()

				// Recover and record stack traces in case of a panic
				if rec := recover(); rec != nil {
					log.Error().
						Str("type", "error").
						Timestamp().
						Interface("recover_info", rec).
						Bytes("debug_stack", debug.Stack()).
						Msg("log system error")
					http.Error(ww, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}

				// log end request
				log.Info().
					Str("type", "access").
					Timestamp().
					Fields(map[string]interface{}{
						"remote_ip":  r.RemoteAddr,
						"url":        r.URL.Path,
						"proto":      r.Proto,
						"method":     r.Method,
						"user_agent": r.Header.Get("User-Agent"),
						"status":     ww.Status(),
						"latency_ms": float64(t2.Sub(t1).Nanoseconds()) / 1000000.0,
						"bytes_in":   r.Header.Get("Content-Length"),
						"bytes_out":  ww.BytesWritten(),
					}).
					Msg("incoming_request")
			}()

			next.ServeHTTP(ww, r)
		}
		return http.HandlerFunc(fn)
	}
}
