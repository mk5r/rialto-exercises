package storage

import (
	"os"
	"path/filepath"
	"reflect"
	"testing"
	"time"

	"gitlab.com/rialtic-external/exercises/v2/person"
)

const testPath = "testDataStore"

func TestDataStore_WritePerson(t *testing.T) {
	type args struct {
		p person.Person
	}
	type result struct {
		hash string
	}
	tests := []struct {
		name    string
		args    args
		result  result
		wantErr bool
	}{
		{
			name: "Should Work Fine",
			args: args{
				p: person.Person{
					FirstName: "John",
					LastName:  "Bologna",
					BirthDate: time.Date(1992, time.July, 2, 0, 0, 0, 0, time.UTC),
				},
			},
			result: result{
				hash: "5d8542f41a8a9d7a6ed6894c4effcea02af0d49a7ef095ceb781d7bffeaa1ebf",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ds := DataStore{
				relativePath: testPath,
			}
			// try to write file
			hash, err := ds.WritePerson(tt.args.p)
			if (err != nil) != tt.wantErr {
				t.Errorf("DataStore.WritePerson() error = %v, wantErr %v", err, tt.wantErr)
			}
			// Check that hash is correct
			if hash != tt.result.hash {
				t.Errorf("DataStore.WritePerson() returned incorrect hash")
			}
			// Check that file got written
			_, err = os.Stat(filepath.Join(".", ds.relativePath, tt.result.hash))
			if err != nil {
				t.Errorf("DataStore.WritePerson() did not create file.")
			}
			// Check file contents
			pGot, err := ds.GetPersonByHash(tt.result.hash)
			if err != nil {
				t.Error("DataStore.WritePerson() test error unable to read result file")
			}
			if pGot != tt.args.p {
				t.Error("DataStore.WritePerson() wrote a file that didn't read as same person")
			}
		})
	}
}

func TestDataStore_GetPersonByHash(t *testing.T) {
	type args struct {
		hash string
	}
	tests := []struct {
		name    string
		args    args
		want    person.Person
		wantErr bool
	}{
		{
			name: "Should Work Fine",
			args: args{
				hash: "5d8542f41a8a9d7a6ed6894c4effcea02af0d49a7ef095ceb781d7bffeaa1ebf",
			},
			want: person.Person{
				FirstName: "John",
				LastName:  "Bologna",
				BirthDate: time.Date(1992, time.July, 2, 0, 0, 0, 0, time.UTC),
			},
			wantErr: false,
		},
		{
			name: "File Doesn't Exist",
			args: args{
				hash: "NOTAREALHASH",
			},
			want:    person.Person{},
			wantErr: true,
		},
		{
			name: "File Has Malformed JSON",
			args: args{
				hash: "badjson",
			},
			want:    person.Person{},
			wantErr: true,
		},
		{
			name: "File Has Incomplete JSON",
			args: args{
				hash: "incompletejson",
			},
			want:    person.Person{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ds := DataStore{
				relativePath: testPath,
			}
			got, err := ds.GetPersonByHash(tt.args.hash)
			if (err != nil) != tt.wantErr {
				t.Errorf("DataStore.GetPersonByHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DataStore.GetPersonByHash() = %v, want %v", got, tt.want)
			}
		})
	}
}
