package storage

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/rialtic-external/exercises/v2/person"
)

type DataStore struct {
	relativePath string
}

func New(relativePath string) (DataStore, error) {
	newPath := filepath.Join(".", relativePath)
	err := os.MkdirAll(newPath, os.ModePerm)
	if err != nil {
		return DataStore{}, fmt.Errorf("unable to create directory at %v: %w", newPath, err)
	}
	return DataStore{
		relativePath: newPath,
	}, nil
}

func (ds DataStore) WritePerson(p person.Person) (string, error) {
	jsonPerson, err := json.Marshal(p)
	if err != nil {
		return "", fmt.Errorf("unable to marshal person: %w", err)
	}

	hash := person.GetHashFromInfo(p)
	filePath := filepath.Join(".", ds.relativePath, hash)

	err = os.WriteFile(filePath, jsonPerson, os.ModePerm)
	if err != nil {
		return "", fmt.Errorf("unable to write file '%s': %w", filePath, err)
	}
	return hash, nil
}

func (ds DataStore) GetPersonByHash(hash string) (person.Person, error) {
	filePath := filepath.Join(".", ds.relativePath, hash)
	pJson, err := os.ReadFile(filePath)
	if err != nil {
		return person.Person{}, fmt.Errorf("unable to read file '%v': %w", filePath, err)
	}

	var p person.Person
	err = json.Unmarshal(pJson, &p)
	if err != nil {
		return person.Person{}, fmt.Errorf("unable to unmarshal json: %w", err)
	}
	return p, nil
}
