package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/rialtic-external/exercises/v2/person"
	"gitlab.com/rialtic-external/exercises/v2/storage"
)

func GetGetPersonHandler(ds storage.DataStore) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		personHash := chi.URLParam(r, "personHash")
		if personHash == "" {
			http.Error(w, "Empty URL Parameter", http.StatusBadRequest)
			return
		}

		person, err := ds.GetPersonByHash(personHash)
		if err != nil {
			http.Error(w, "Unable to get person", http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(person)
	}
}

func GetPutPersonHandler(ds storage.DataStore) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		data := &person.Person{}
		err := json.NewDecoder(r.Body).Decode(data)
		if err != nil {
			http.Error(w, "Unable to decode request body", http.StatusBadRequest)
			return
		}

		hash, err := ds.WritePerson(*data)
		if err != nil {
			http.Error(w, "Error writing person", http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(hash))
	}
}
