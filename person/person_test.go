package person

import (
	"testing"
	"time"
)

func TestGetHashFromInfo(t *testing.T) {
	tests := []struct {
		name string
		p    Person
		want string
	}{
		{
			name: "Should Work Fine",
			p: Person{
				FirstName: "John",
				LastName:  "Bologna",
				BirthDate: time.Date(1992, time.July, 2, 0, 0, 0, 0, time.UTC),
			},
			want: "5d8542f41a8a9d7a6ed6894c4effcea02af0d49a7ef095ceb781d7bffeaa1ebf",
		},
		// TODO: come up with test cases. Might be weird to do though? Maybe a string that's too long?
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetHashFromInfo(tt.p); got != tt.want {
				t.Errorf("GetHashFromInfo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCheckHashCorrect(t *testing.T) {
	type args struct {
		p Person
		h string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Should Work Fine",
			args: args{
				p: Person{
					FirstName: "John",
					LastName:  "Bologna",
					BirthDate: time.Date(1992, time.July, 2, 0, 0, 0, 0, time.UTC),
				},
				h: "5d8542f41a8a9d7a6ed6894c4effcea02af0d49a7ef095ceb781d7bffeaa1ebf",
			},
			want: true,
		},
		{
			name: "Hash Is Wrong",
			args: args{
				p: Person{
					FirstName: "John",
					LastName:  "JKAJKIKEKDIOSOSDUIGU",
					BirthDate: time.Date(1992, time.July, 2, 0, 0, 0, 0, time.UTC),
				},
				h: "5d8542f41a8a9d7a6ed6894c4effcea02af0d49a7ef095ceb781d7bffeaa1ebf",
			},
			want: false,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CheckHashCorrect(tt.args.p, tt.args.h); got != tt.want {
				t.Errorf("CheckHashCorrect() = %v, want %v", got, tt.want)
			}
		})
	}
}
