package person

import (
	"crypto/sha256"
	"fmt"
	"time"
)

type Person struct {
	FirstName string
	LastName  string
	BirthDate time.Time
}

func GetHashFromInfo(p Person) string {
	hash := sha256.Sum256([]byte(fmt.Sprintf("%#v", p)))
	hashString := fmt.Sprintf("%x", hash)
	return hashString
}

func CheckHashCorrect(p Person, h string) bool {
	return h == GetHashFromInfo(p)
}
