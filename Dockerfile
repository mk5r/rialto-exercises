FROM golang:1.20-alpine as build
ENV CGO_ENABLED=0

WORKDIR /go/src/app

COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN go vet -v
RUN go test -v

RUN go build -o /go/bin/app

FROM gcr.io/distroless/static-debian11:nonroot

COPY --from=build /go/bin/app /
CMD ["/app"]