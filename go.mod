module gitlab.com/rialtic-external/exercises/v2

go 1.20

require (
	github.com/go-chi/chi v1.5.4
	github.com/rs/zerolog v1.29.0
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)

require (
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.0.0-20210927094055-39ccf1dd6fa6 // indirect
)
