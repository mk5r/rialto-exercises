# EXPLANATION

Here I'll document my process and thoughts as I move through the coding challenge.

## Initial Thoughts

Seems fairly straightforward, all things considered: 
- cluster/infra:
  - local k8s
  - nginx for ingress, linkerd for service mesh. 
    - [integration instructions](https://linkerd.io/2.12/tasks/using-ingress/#nginx)
      - specific instructions on annotation. Seems to be compatible with regular helm chart w/ some specifics.
  - Some terraform, either to push the manifests, or to set up IRSA.
    - Skipping, referencing portfolio work.
    - To see me using IRSA more intentionally, see:
      - https://github.com/mkantzer/k8splayground-core-infra
      - https://github.com/mkantzer/k8splayground-cluster-state
- application
  - golang as an option.
  - Simple REST w/ 2 endpoints:
    - /hash PUT -> in: birthday, first name, last name -> out: hash
    - /hash GET -> in: hash -> out: birthday, first name, last name
      - Going to require some kind of data storage that's not just keeping values in memory.
        - Ideally a database, but I'm not sure I really feel like dealing with that right now.
        - If I'm writing JSON to a file, I'll need to make sure I'm putting it in a `PersistentVolume` instead of just the pod's fs.
          - Probably the most straight-forward option.
          - Will need to make sure unit testing is injectable.
          - Will need to make sure I can unit test the file read.
          - Probably _could_ use this as an excuse to play around with cuelang for data validation, but that feels like an "after I've done this" kind of thing.
        - Could get _really_ cheeky and store stuff in a configmap (_not_ writing a CRD for this).
          - NOT "mount a configmap as a volume", but make API requests against it.
          - Would require some RBAC
          - may make unit-testing at least a _little_ more annoying.
          - May end up requiring integration tests as well...
  - UNIT TEST IT.
- CI
  - container build
  - Run unit tests
  - container push w/ tags
  - semver release

I'll probably make a few modifications to the _specific_ instructions to better fit my working style and preferred tools, but that shouldn't be particularly wild.

Likely notable changes:
- using `kind` instead of docker desktop for cluster creation.
  - Better repeatability, better portability.
  - Possibility for also running full end-to-end tests _in k8s_ in CI. 
    - Not a huge fan of doing this for a whole system, but it's certainly workable for a single microservice.
- Using `skaffold` for the build/test/deploy cycles. 
  - Would prefer to use [`ko` instead of `docker` for the build](https://skaffold.dev/docs/builders/builder-types/ko/), as it gets a bunch of stuff for free. Alas, I'll be using docker.



Extras & Questions:
1. how did you handle logging? why?
   - The primary thing I wanted to get across was an emphasis on logging JSON to STDOUT: it's the right way to push out logs from a 12 factor standpoint, and is the easiest to manage in k8s. In a real cluster, those logs would also get piped to something like elasticsearch or some other aggregator. By structuring logs, you make them significantly easier to search and manage.
   - I didn't take the time to actually dependancy-inject the logging framework into the request handlers, but it would be straightforward to do so. 
2. Do the unit tests pass? Why, Why not?
   - Yes, but I'm not particularly happy with them. I'm not testing the handlers, nor am I doing a particularly good job of testing the datastore. With more time, I would have implemented sufficient mocking to do so. 
3. What are some possible issues with this design? would you deploy this in production? why, why not?
   - First and foremost is how I'm storing data. A real application should store this kind of data in a database. I used a mounted volume for speed and limiting application complexity, but this comes with _huge_ tradeoffs in terms of scalability and operability: you're limited to one copy of the application, you don't have anything _backing_ the store that could provide use-case specific help, you don't get any computation benefits from using a database table lookup. This information is also PII, and should be handled with security as a first principal, including encryption at rest and in transit. 
   - I'm not implementing any kind of HTTPS/TLS. Even were we _not_ dealing with PII, this would be unacceptable. The decision to not use it was to limit complexity, and needing to deal with any kind of PKI with the client, as I won't be using an owned host-name.
   - I haven't set any kind of resource requests or limits.
   - `kind` is _not_ a production-grade k8s distribution.
   - If this was the only app I needed to run, I wouldn't even bother with k8s. I'd just throw it into Fargate (or GKE's equivalent). 
   - I'm not versioning my datastore schema, which will make updating it difficult.
   - Using the file system without any kind of channeling means I could end up with race conditions if multiple writes to the same file happen concurrently.
   - I'm not a fan of `push` styles of kubernetes manifest management. I'd instead implement this as a gitops pull, via argocd. 
   - I'd store linkerd's PKI somewhere that's actually scoped for dealing with secrets.
   - Frankly, I haven't written the application particularly well. 
4. How would you scale this to 10 requests/second? How about 100 requests/second?
   - Honestly, I'm fairly sure the current setup could handle that fairly well if run on. Outside of networking, the biggest bottleneck will be disk i/o. This could be optimized in a bunch of ways, depending on how I end up implementing this (I'm pre-answering some of this):
     - write each entry to a dedicated file instead of a single huge JSON file, w/ the hash as the name. This would make parallelization, better, decrease read/write time, and decrease _lookup_ time. Actually, this is probably how I'm going to do the storage.
     - use an actual database instead of this dumb system. 
      - Will also enable horizontal scaling.
     - cache results in-memory, or in something like a REDIS (absolute overkill for this).
5. How could we improve this scenario?
   - Overall, I think this is a pretty solid exercise. It touches enough components in interesting enough ways to see how an engineer thinks, without being overly complex that it's a burden. I wasn't expecting to integrate linkerd into something at this level, but it makes sense given the need for encryption-in-transit for HIPAA. I was thinking that I'd want a more explicit call-out for not expecting production-grade results, but I now think it's actually useful to see how critical a candidate can be towards their own designs.